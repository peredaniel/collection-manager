//
//  ListProtocol.swift
//  Model Collection
//
//  Created by Pere Daniel Prieto on 11/6/18.
//  Copyright © 2018 BaseTIS. All rights reserved.
//

import UIKit
import Foundation

protocol ListModel: class {
    var name: String { get }
    var status: String { get }
    var image: UIImage? { get }
}

protocol ListCategory: class {
    var name: String { get }
}
