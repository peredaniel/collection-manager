//
//  DataManagerProtocol.swift
//  Model Collection
//
//  Created by Pere Daniel Prieto on 11/6/18.
//  Copyright © 2018 BaseTIS. All rights reserved.
//

import Foundation

protocol DataManagerProtocol: class {
    func getCategories() -> [ListCategory]?
    func getNumberOfModels(in category: Category) -> Int
    func getModels(in category: Category) -> [ListModel]
    func getModels() -> [[ListModel]]
}
