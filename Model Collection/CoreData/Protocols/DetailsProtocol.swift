//
//  DetailsProtocol.swift
//  Model Collection
//
//  Created by Pere Daniel Prieto on 11/6/18.
//  Copyright © 2018 BaseTIS. All rights reserved.
//

import UIKit
import Foundation

protocol DetailModel: ListModel {
    var categoryName: String { get }
    var modelDescription: String? { get }
}

protocol DetailCategory: ListCategory {
    var categoryDescription: String? { get }
}
