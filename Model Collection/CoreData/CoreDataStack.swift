//
//  CoreDataStack.swift
//  SWLArmyBuilder
//
//  Created by Pedro Daniel Prieto Martínez on 13/1/18.
//  Copyright © 2018 Pedro Daniel Prieto Martínez. All rights reserved.
//

import UIKit
import CoreData

open class CoreDataStack {
        
    private let coreDataModelFileName = "CollectionModel"
    private let coreDataModelExtension = "momd"
    private let sqliteExtension = ".sqlite"
    private let sqliteWalExtension = ".sqlite-wal"
    private let sqliteShmExtension = ".sqlite-shm"
    private let persistentStoreOptions = [NSMigratePersistentStoresAutomaticallyOption: true, NSInferMappingModelAutomaticallyOption: true]
    
    static let shared = CoreDataStack()
    
    private init() {
        
    }
    
    fileprivate lazy var managedObjectContext: NSManagedObjectContext = {
        let context = NSManagedObjectContext(concurrencyType: .mainQueueConcurrencyType)
        context.persistentStoreCoordinator = persistentStoreCoordinator
        return context
    }()
    
    fileprivate lazy var managedObjectModel: NSManagedObjectModel = {
        let modelURL = Bundle.main.url(forResource: coreDataModelFileName, withExtension: coreDataModelExtension)!
        return NSManagedObjectModel(contentsOf: modelURL)!
    }()
    
    fileprivate lazy var persistentStoreCoordinator: NSPersistentStoreCoordinator = {
        let storeURL = applicationDocumentsDirectory.appendingPathComponent(coreDataModelFileName + sqliteExtension)
        let coordinator = NSPersistentStoreCoordinator(managedObjectModel: managedObjectModel)
        do {
            try coordinator.addPersistentStore(ofType: NSSQLiteStoreType, configurationName: nil, at: storeURL, options: persistentStoreOptions)
        }
        catch {
            fatalError("Could not add the persistent store: \(error).")
        }
        
        return coordinator
    }()
    
    public var applicationDocumentsDirectory: URL {
        return FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).last!
    }
    
    fileprivate func deletePersistentFiles() {
        let sqliteURL = applicationDocumentsDirectory.appendingPathComponent(coreDataModelFileName + sqliteExtension)
        let sqliteWalURL = applicationDocumentsDirectory.appendingPathComponent(coreDataModelFileName + sqliteWalExtension)
        let sqliteShmURL = applicationDocumentsDirectory.appendingPathComponent(coreDataModelFileName + sqliteShmExtension)
        do {
            try persistentStoreCoordinator.remove(persistentStoreCoordinator.persistentStore(for: sqliteURL)!)
	        try FileManager.default.removeItem(at: sqliteURL)
            try FileManager.default.removeItem(at: sqliteWalURL)
            try FileManager.default.removeItem(at: sqliteShmURL)
        }
        catch {
            NSLog("Could not erase files: \(error).", "")
        }
        do {
            try persistentStoreCoordinator.addPersistentStore(ofType: NSSQLiteStoreType, configurationName: nil, at: sqliteURL, options: persistentStoreOptions)
        }
        catch {
            fatalError("Could not add the persistent store: \(error).")
        }
    }
    
}

public extension CoreDataStack {
    
    public func insert(_ entityName: String) -> NSManagedObject {
        return NSEntityDescription.insertNewObject(forEntityName: entityName, into: self.managedObjectContext)
    }
    
    public func delete(_ object: NSManagedObject) {
        managedObjectContext.delete(object)
    }
    
    public func fetchObjects(_ fetchRequest: NSFetchRequest<NSManagedObject>) -> [NSManagedObject] {
        do {
            let results = try managedObjectContext.fetch(fetchRequest)
            return results
        } catch {
            fatalError("Could not fetch the requested objects: \(error).")
        }
    }
    
    public func saveContext() {
        if managedObjectContext.hasChanges {
            do { try managedObjectContext.save() }
            catch {
                NSLog("\(error)")
                managedObjectContext.rollback()
                
            }
        }
    }
    
    public func deleteCoreData() {
        let storePath = applicationDocumentsDirectory.appendingPathComponent(coreDataModelFileName + sqliteExtension).path
        if FileManager.default.fileExists(atPath: storePath) {
            deletePersistentFiles()
        } else {
            NSLog("Files do not exist at the location: %@", storePath)
        }
    }
    
    public func coreDataFilesSize() -> Double {
        var totalSize: Double = 0.0
        do {
            var fileDictionary = try FileManager.default.attributesOfItem(atPath: applicationDocumentsDirectory.appendingPathComponent(coreDataModelFileName + sqliteExtension).path)
            totalSize += (fileDictionary[FileAttributeKey.size] as? Double)!
            fileDictionary = try FileManager.default.attributesOfItem(atPath: applicationDocumentsDirectory.appendingPathComponent(coreDataModelFileName + sqliteWalExtension).path)
            totalSize += (fileDictionary[FileAttributeKey.size] as? Double)!
            fileDictionary = try FileManager.default.attributesOfItem(atPath: applicationDocumentsDirectory.appendingPathComponent(coreDataModelFileName + sqliteShmExtension).path)
            totalSize += (fileDictionary[FileAttributeKey.size] as? Double)!
        }
        catch {
            NSLog("Files do not exist at the location: %@", applicationDocumentsDirectory.path)
        }
        return totalSize
    }
    
    public func logCoreDataFilesSize(withTitle title: String) {
        let size = String(coreDataFilesSize()/1024.0) + " kb"
        let finalLog = "\(title): \(size)"
        NSLog("%@", finalLog)
    }
}

