//
//  MockLoader.swift
//  Model Collection
//
//  Created by Pere Daniel Prieto on 11/6/18.
//  Copyright © 2018 BaseTIS. All rights reserved.
//

import Foundation
import UIKit

enum MockLoader {
    static func load() {
        Categories.load()
    }
}

enum Categories {
    static func load() {
        Revell.load()
        Citadel.load()
        AMT.load()
    }
}

struct Revell {
    static func load() {
        let revell = Category.make(name: "Revell", description: "Revell has a great easy-to-build kit!")
        revell.add(model: StarDestroyer.load())
        revell.add(model: ARC170.load())
        revell.add(model: TieAdvanced.load())
        CoreDataStack.shared.saveContext()
    }
}

struct Citadel {
    static func load() {
        let citadel = Category.make(name: "Citadel", description: "Some of the greatest fantasy models")
        citadel.add(model: HighElfDragon.load())
        CoreDataStack.shared.saveContext()
    }
}

struct AMT {
    static func load() {
        _ = Category.make(name: "AMT", description: "They have awesome Star Trek models. I'm thinking to buy some in the future.")
        CoreDataStack.shared.saveContext()
    }
}

struct StarDestroyer {
    static func load() -> Model {
        return Model.make(name: "Imperial-class Star Destroyer", status: "Painted", image: UIImage(named: "starDestroyerRevell"), description: "An outstanding model! My favourite!")
    }
}

struct ARC170 {
    static func load() -> Model {
        return Model.make(name: "ARC-170", status: "Assembled")
    }
}

struct TieAdvanced {
    static func load() -> Model {
        return Model.make(name: "TIE Advanced x1", status: "Painted", description: "The personal starfighter of Lord Darth Vader. In designing the starfighters of its TIE line, Sienar Fleet Systems drew heavily on the designs of Kuat Systems Engineering ships such as the V-wing and Eta-2 interceptor. Sienar also experimented with advanced models featuring localized improvements and secret technological breakthroughs, including the TIE Advanced v1 prototype, based on Sienar's Scimitar Star Courier. This prototype led to the TIE Advanced x1, which boasted a hyperdrive and deflector shield generator. A modified early prototype of the Advanced x1 line was flown by Darth Vader himself. Vader's fighter boasted greater speed and heavier firepower, featuring fixed-mounted twin blaster cannons and a cluster missile launcher. Vader specified a custom cockpit to accommodate his armored suit, and high-performance solar cells were fitted into the fighter's curved wings.")
    }
}

struct HighElfDragon {
    static func load() -> Model {
        return Model.make(name: "High Elf Prince in Dradon", status: "To assemble", image: nil, description: nil)
    }
}
