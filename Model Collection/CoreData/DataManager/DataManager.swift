//
//  DataManager.swift
//  Model Collection
//
//  Created by Pere Daniel Prieto on 11/6/18.
//  Copyright © 2018 BaseTIS. All rights reserved.
//

import UIKit
import CoreData

class DataManager {
    static let shared = DataManager()
    
    private init() { }
    
}

extension DataManager: DataManagerProtocol {
    func getNumberOfModels(in category: Category) -> Int {
        return category.numberOfModels
        
    }
    
    func getModels(in category: Category) -> [ListModel] {
        return Array(category.models)
    }
    
    func getModels() -> [[ListModel]] {
        var finalArray = [[ListModel]]()
        if let categories = getCategories() {
            for category in categories {
                finalArray.append(getModels(in: category as! Category))
            }
        }
        return finalArray
    }
    
    func getCategories() -> [ListCategory]? {
        let request = NSFetchRequest<NSManagedObject>(entityName: Category.entityName)
        return CoreDataStack.shared.fetchObjects(request) as? [ListCategory]
    }
}
