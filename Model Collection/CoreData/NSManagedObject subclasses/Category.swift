//
//  Category.swift
//  Model Collection
//
//  Created by Pere Daniel Prieto on 11/6/18.
//  Copyright © 2018 BaseTIS. All rights reserved.
//

import UIKit
import CoreData

class Category: NSManagedObject {

    class var entityName: String {
        return "Category"
    }
    
    var numberOfModels: Int {
        return models.count
    }
    
    class func make(name: String, description: String?=nil) -> Category {
        let category = CoreDataStack.shared.insert(entityName) as! Category
        
        category.name = name
        category.categoryDescription = description
        
        return category
    }
    
    func add(model: Model) {
        model.category = self
        addToModels(model)
    }
    
}

extension Category: ListCategory, DetailCategory {
    
    @NSManaged public var name: String
    @NSManaged public var categoryDescription: String?
    @NSManaged public var models: Set<Model>
    
    @objc(addModelsObject:)
    @NSManaged public func addToModels(_ value: Model)
    
    @objc(removeModelsObject:)
    @NSManaged public func removeFromModels(_ value: Model)
    
    @objc(addModels:)
    @NSManaged public func addToModels(_ values: Set<Model>)
    
    @objc(removeModels:)
    @NSManaged public func removeFromModels(_ values: Set<Model>)
    
}
