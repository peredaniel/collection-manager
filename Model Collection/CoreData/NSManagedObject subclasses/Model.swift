//
//  Model.swift
//  Model Collection
//
//  Created by Pere Daniel Prieto on 11/6/18.
//  Copyright © 2018 BaseTIS. All rights reserved.
//

import UIKit
import CoreData

class Model: NSManagedObject, ListModel, DetailModel {

    class var entityName: String {
        return "Model"
    }
    
    var image: UIImage? {
        if let encodedImage = encodedImage {
            return UIImage(data: encodedImage)
        } else {
            return nil
        }
    }
    
    var categoryName: String {
        return category.name
    }

    class func make(name: String, status: String, image: UIImage?=nil, description: String?=nil) -> Model {
        let model = CoreDataStack.shared.insert(entityName) as! Model
        
        model.name = name
        model.status = status
        model.modelDescription = description
        if let image = image {
	        model.encodedImage = UIImagePNGRepresentation(image)
        } else {
            model.encodedImage = nil
        }
        
        return model
    }
    
    func add(to category: Category) {
        category.add(model: self)
    }
    
}

extension Model {
  
    @NSManaged public var name: String
    @NSManaged public var modelDescription: String?
    @NSManaged public var status: String
    @NSManaged public var encodedImage: Data?
    @NSManaged public var category: Category
    
}
